package com.example.recucorte2;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
    private EditText editTextAltura;
    private EditText editTextPeso;
    private TextView textViewIMC;
    private com.example.recucorte2.DbHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbHelper = new com.example.recucorte2.DbHelper(this);

        editTextAltura = findViewById(R.id.editTextAltura);
        editTextPeso = findViewById(R.id.editTextPeso);
        textViewIMC = findViewById(R.id.textViewIMC);

        Button buttonCalcular = findViewById(R.id.btnCalcular);
        buttonCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcularIMC();

            }
        });

        Button buttonGuardar = findViewById(R.id.btnGuardar);
        buttonGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guardarIMC();
                editTextAltura.setText("");
                editTextPeso.setText("");
            }
        });

        Button buttonVerHistorial = findViewById(R.id.btnVerHistorial);
        buttonVerHistorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verHistorial();
            }
        });

        Button buttonSalir = findViewById(R.id.btnSalir);
        buttonSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void calcularIMC() {
        String alturaStr = editTextAltura.getText().toString().trim();
        String pesoStr = editTextPeso.getText().toString().trim();

        if (alturaStr.isEmpty() || pesoStr.isEmpty()) {
            Toast.makeText(this, "Por favor, ingresa la altura y el peso.", Toast.LENGTH_SHORT).show();
            return;
        }

        double altura = Double.parseDouble(alturaStr);
        double peso = Double.parseDouble(pesoStr);

        double imc = peso / ((altura / 100) * (altura / 100));
        textViewIMC.setText("IMC: " + String.format("%.2f", imc));
    }

    private void guardarIMC() {
        String alturaStr = editTextAltura.getText().toString().trim();
        String pesoStr = editTextPeso.getText().toString().trim();
        String imcStr = textViewIMC.getText().toString().trim().replace("IMC: ", "");

        if (alturaStr.isEmpty() || pesoStr.isEmpty() || imcStr.isEmpty()) {
            Toast.makeText(this, "Por favor, calcula el IMC antes de guardar.", Toast.LENGTH_SHORT).show();
            return;
        }

        double altura = Double.parseDouble(alturaStr);
        double peso = Double.parseDouble(pesoStr);
        double imc = Double.parseDouble(imcStr);

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(com.example.recucorte2.DbContract.HistorialEntry.COLUMN_ALTURA, altura);
        values.put(com.example.recucorte2.DbContract.HistorialEntry.COLUMN_PESO, peso);
        values.put(com.example.recucorte2.DbContract.HistorialEntry.COLUMN_IMC, imc);

        long newRowId = db.insert(com.example.recucorte2.DbContract.HistorialEntry.TABLE_NAME, null, values);

        if (newRowId != -1) {
            Toast.makeText(this, "IMC guardado correctamente.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Error al guardar el IMC.", Toast.LENGTH_SHORT).show();
        }

        db.close();
    }

    private void verHistorial() {
        Intent intent = new Intent(MainActivity.this, com.example.recucorte2.HistorialActivity.class);
        startActivity(intent);
    }
}